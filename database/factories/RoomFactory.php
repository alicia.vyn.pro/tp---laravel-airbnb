<?php

use Faker\Generator as Faker;

$factory->define(App\Room::class, function (Faker $faker) {
    return [
        'title' => $faker->sentence(4),
        'size' => $faker->numberBetween(10, 120),
        'price' => $faker->numberBetween(25, 250),
        'bedding' => $faker->randomDigitNotNull,
        'description' => $faker->paragraph(3, true),
        'type_id' => App\Type::all()->random()->id,
        'address_id' => App\Address::all()->random()->id,
        'user_id' => App\User::all()->random()->id,
    ];
});
