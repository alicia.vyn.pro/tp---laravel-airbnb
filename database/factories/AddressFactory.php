<?php

use Faker\Generator as Faker;

$factory->define(App\Address::class, function (Faker $faker) {
    return [
        'numero' => $faker->buildingNumber,
        'rue' => $faker->streetName,
        'cp' => $faker->postcode,
        'ville' => $faker->city,
        'pays' => $faker->country,
    ];
});
