<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateEquipmentRoomTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('equipment_room', function (Blueprint $table) {

            $table->foreign('room_id')
                ->references('id')
                ->on('rooms');

            $table->foreign('equipment_id')
                ->references('id')
                ->on('equipment');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('equipment_room', function (Blueprint $table) {
            $table->dropForeign(['room_id']);
            $table->dropForeign(['equipment_id']);
        });
    }
}
