<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reservation extends Model
{
    protected $fillable = [
        'user_id', 'room_id', 'date_start', 'date_end'
    ];

    function user() {
        return $this->belongsTo( User::class );
    }

    function room() {
        return $this->belongsTo( Room::class );
    }
}
