<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    protected $fillable = [
        'numero', 'rue', 'cp', 'ville', 'pays'
    ];

    function rooms() {
        return $this->hasOne( Room::class );
    }
}
