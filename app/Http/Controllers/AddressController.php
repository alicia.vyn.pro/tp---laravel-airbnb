<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AddressController extends Controller
{
    public function store(Request $request, Address $address)
    {
        $fields = $request->validate([
            'numero' => 'nullable',
            'rue' => 'nullable',
            'cp' => 'nullable',
            'ville' => 'required',
            'pays' => 'required'
        ]);
    }
}
