<?php

namespace App\Http\Controllers;

use App\Reservation;
use App\Room;
use Auth;
use Illuminate\Http\Request;

class ReservationController extends Controller
{

    /**
     * Display the specified resource.
     *
     * @param  \App\Reservation  $reservation
     * @return \Illuminate\Http\Response
     */
    public function index(Reservation $reservation)
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Room $room)
    {
        $fields = $request->validate([
            'date_start' => 'required',
            'date_end' => 'required',
        ]);

        $fields['user_id'] = Auth::user()->id;
        $fields['room_id'] = $room->id;

        $reservation = Reservation::create( $fields );

        return redirect(route('reservations.show'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Reservation  $reservation
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        $reservations = Auth::user()->reserved;

        return view('reservations.show', compact('reservations'));
    }


}
