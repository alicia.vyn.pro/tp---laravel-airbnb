<?php

namespace App\Http\Controllers;

use App\Equipment;
use App\Room;
use App\Type;
use App\Address;
use Auth;
use Illuminate\Http\Request;

class RoomController extends Controller
{
    function __construct()
    {
        $this->middleware('auth', ['except' => ['index', 'show']]);
        $this->middleware('check.announcer', ['only' => ['create', 'store']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $rooms = Room::all();
        return view('rooms.index', compact('rooms'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $types = Type::all();
        $equipments = Equipment::all();
        return view('rooms.create', compact('types', 'equipments'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $fields = $request->validate([
            'title' => 'required|max:100',
            'size' => 'required',
            'price' => 'required',
            'bedding' => 'required',
            'numero' => 'nullable',
            'rue' => 'nullable',
            'cp' => 'nullable',
            'ville' => 'required',
            'pays' => 'required',
            'description' => 'nullable',
            'type_id' => 'required',
            'equipments' => 'nullable',
        ]);

        $address = Address::create( $fields );

        $fields['user_id'] = Auth::user()->id;
        $fields['address_id'] = $address->id;

        $room = Room::create( $fields );
        $room->addEquipments( $fields['equipments'] );

        return redirect( route('rooms.show', $room) );

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Rooms  $rooms
     * @return \Illuminate\Http\Response
     */
    public function show(Room $room)
    {
        $equips = Equipment::all();
        return view('rooms.show', compact('room', 'equips'));
    }

}
