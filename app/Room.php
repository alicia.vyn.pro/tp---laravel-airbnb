<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Room extends Model
{
    protected $fillable = [
        'title', 'size', 'price', 'bedding', 'description', 'type_id', 'address_id', 'user_id'
    ];

    function user() {
        return $this->belongsTo( User::class );
    }

    function address() {
        return $this->belongsTo( Address::class );
    }

    function type() {
        return $this->belongsTo(Type::class );
    }

    function reserved() {
        return $this->hasMany( Reservation::class );
    }

    function equipments() {
        return $this->belongsToMany(Equipment::class );
    }

    function addEquipments(array $equipment) {
        $this->equipments()->attach($equipment);
    }

    function hasEquipment( $id ) {
        return $this->equipments->contains($id);
    }

}
