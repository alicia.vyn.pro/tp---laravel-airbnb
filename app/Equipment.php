<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Equipment extends Model
{
    function rooms() {
        return $this->belongsToMany( Room::class );
    }
}
