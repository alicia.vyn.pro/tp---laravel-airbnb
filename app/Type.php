<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Type extends Model
{
    function rooms() {
        return $this->hasMany( Room::class );
    }
}
