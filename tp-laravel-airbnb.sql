-- --------------------------------------------------------
-- Hôte :                        localhost
-- Version du serveur:           5.7.24 - MySQL Community Server (GPL)
-- SE du serveur:                Win64
-- HeidiSQL Version:             9.5.0.5332
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Listage de la structure de la base pour tp-laravel-airbnb
CREATE DATABASE IF NOT EXISTS `tp-laravel-airbnb` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `tp-laravel-airbnb`;

-- Listage de la structure de la table tp-laravel-airbnb. addresses
CREATE TABLE IF NOT EXISTS `addresses` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `numero` int(10) unsigned DEFAULT NULL,
  `rue` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cp` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ville` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pays` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table tp-laravel-airbnb.addresses : ~7 rows (environ)
/*!40000 ALTER TABLE `addresses` DISABLE KEYS */;
INSERT INTO `addresses` (`id`, `numero`, `rue`, `cp`, `ville`, `pays`, `created_at`, `updated_at`) VALUES
	(1, 949, 'Jarred Forges', '99076', 'Bartellfurt', 'Japan', '2019-02-20 12:04:01', '2019-02-20 12:04:01'),
	(2, 9702, 'Emard Hills', '4549', 'North Oralborough', 'Guyana', '2019-02-20 12:04:01', '2019-02-20 12:04:01'),
	(3, 180, 'Lilly Ports', '95751-9294', 'West Zelma', 'Nigeria', '2019-02-20 12:06:21', '2019-02-20 12:06:21'),
	(4, 75136, 'Russel Lodge', '98881-8948', 'East Garnet', 'Peru', '2019-02-20 12:06:21', '2019-02-20 12:06:21'),
	(5, 58238, 'Schuppe Fort', '82561-0322', 'Lake Justine', 'Anguilla', '2019-02-20 12:06:21', '2019-02-20 12:06:21'),
	(6, 286, 'Olin Hill', '27296', 'Earltown', 'Macedonia', '2019-02-20 12:06:21', '2019-02-20 12:06:21'),
	(7, 6142, 'Larkin Route', '57298', 'Vladimirhaven', 'Monaco', '2019-02-20 12:06:21', '2019-02-20 12:06:21'),
	(8, 13, 'rue Beauvau', '13001', 'Marseille', 'France', '2019-02-20 12:11:59', '2019-02-20 12:11:59');
/*!40000 ALTER TABLE `addresses` ENABLE KEYS */;

-- Listage de la structure de la table tp-laravel-airbnb. equipment
CREATE TABLE IF NOT EXISTS `equipment` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `label` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table tp-laravel-airbnb.equipment : ~2 rows (environ)
/*!40000 ALTER TABLE `equipment` DISABLE KEYS */;
INSERT INTO `equipment` (`id`, `label`, `created_at`, `updated_at`) VALUES
	(1, 'Machine à laver', '2019-02-20 12:57:25', '2019-02-20 12:57:25'),
	(2, 'Sèche linge', NULL, NULL),
	(3, 'Télévision', NULL, NULL);
/*!40000 ALTER TABLE `equipment` ENABLE KEYS */;

-- Listage de la structure de la table tp-laravel-airbnb. equipment_room
CREATE TABLE IF NOT EXISTS `equipment_room` (
  `room_id` int(10) unsigned NOT NULL,
  `equipment_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  UNIQUE KEY `equipment_room_room_id_equipment_id_unique` (`room_id`,`equipment_id`),
  KEY `equipment_room_equipment_id_foreign` (`equipment_id`),
  CONSTRAINT `equipment_room_equipment_id_foreign` FOREIGN KEY (`equipment_id`) REFERENCES `equipment` (`id`),
  CONSTRAINT `equipment_room_room_id_foreign` FOREIGN KEY (`room_id`) REFERENCES `rooms` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table tp-laravel-airbnb.equipment_room : ~2 rows (environ)
/*!40000 ALTER TABLE `equipment_room` DISABLE KEYS */;
INSERT INTO `equipment_room` (`room_id`, `equipment_id`, `created_at`, `updated_at`) VALUES
	(6, 1, NULL, NULL),
	(6, 3, NULL, NULL);
/*!40000 ALTER TABLE `equipment_room` ENABLE KEYS */;

-- Listage de la structure de la table tp-laravel-airbnb. migrations
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=63 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table tp-laravel-airbnb.migrations : ~15 rows (environ)
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
	(9, '2019_02_19_082216_create_equipment_rooms_table', 7),
	(12, '2019_02_19_082527_update_equipment_rooms_table', 10),
	(49, '2014_10_12_000000_create_users_table', 11),
	(50, '2014_10_12_100000_create_password_resets_table', 11),
	(51, '2019_02_19_081807_create_roles_table', 11),
	(52, '2019_02_19_081929_create_types_table', 11),
	(53, '2019_02_19_081956_create_addresses_table', 11),
	(54, '2019_02_19_082027_create_rooms_table', 11),
	(55, '2019_02_19_082105_create_equipment_table', 11),
	(56, '2019_02_19_082142_create_reservations_table', 11),
	(57, '2019_02_19_082216_create_equipment_room_table', 11),
	(58, '2019_02_19_082255_update_rooms_table', 11),
	(59, '2019_02_19_082427_update_reservations_table', 11),
	(60, '2019_02_19_082527_update_equipment_room_table', 11),
	(61, '2019_02_20_104912_update_users_table', 11),
	(62, '2019_02_20_110934_update_users_table', 12);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;

-- Listage de la structure de la table tp-laravel-airbnb. password_resets
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table tp-laravel-airbnb.password_resets : ~0 rows (environ)
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;

-- Listage de la structure de la table tp-laravel-airbnb. reservations
CREATE TABLE IF NOT EXISTS `reservations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `room_id` int(10) unsigned NOT NULL,
  `date_start` date NOT NULL,
  `date_end` date NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id_room_id_date_start_date_end` (`user_id`,`room_id`,`date_start`,`date_end`),
  KEY `reservations_room_id_foreign` (`room_id`),
  CONSTRAINT `reservations_room_id_foreign` FOREIGN KEY (`room_id`) REFERENCES `rooms` (`id`),
  CONSTRAINT `reservations_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table tp-laravel-airbnb.reservations : ~1 rows (environ)
/*!40000 ALTER TABLE `reservations` DISABLE KEYS */;
INSERT INTO `reservations` (`id`, `user_id`, `room_id`, `date_start`, `date_end`, `created_at`, `updated_at`) VALUES
	(20, 1, 6, '2019-02-25', '2019-03-04', '2019-02-25 12:34:43', '2019-02-25 12:34:43'),
	(21, 1, 1, '2019-02-25', '2019-03-04', '2019-02-25 12:40:31', '2019-02-25 12:40:31');
/*!40000 ALTER TABLE `reservations` ENABLE KEYS */;

-- Listage de la structure de la table tp-laravel-airbnb. roles
CREATE TABLE IF NOT EXISTS `roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `label` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table tp-laravel-airbnb.roles : ~2 rows (environ)
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` (`id`, `label`, `created_at`, `updated_at`) VALUES
	(1, 'Annonceur', '2019-02-20 12:56:13', NULL),
	(2, 'Utilisateur', NULL, NULL);
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;

-- Listage de la structure de la table tp-laravel-airbnb. rooms
CREATE TABLE IF NOT EXISTS `rooms` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `size` double(8,2) NOT NULL,
  `price` double(8,2) NOT NULL,
  `bedding` int(10) unsigned NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `type_id` int(10) unsigned NOT NULL,
  `address_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `rooms_type_id_foreign` (`type_id`),
  KEY `rooms_address_id_foreign` (`address_id`),
  KEY `rooms_user_id_foreign` (`user_id`),
  CONSTRAINT `rooms_address_id_foreign` FOREIGN KEY (`address_id`) REFERENCES `addresses` (`id`),
  CONSTRAINT `rooms_type_id_foreign` FOREIGN KEY (`type_id`) REFERENCES `types` (`id`),
  CONSTRAINT `rooms_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table tp-laravel-airbnb.rooms : ~5 rows (environ)
/*!40000 ALTER TABLE `rooms` DISABLE KEYS */;
INSERT INTO `rooms` (`id`, `title`, `size`, `price`, `bedding`, `description`, `type_id`, `address_id`, `user_id`, `created_at`, `updated_at`) VALUES
	(1, 'Fugit velit vitae.', 65.00, 102.00, 6, 'Officia officia beatae veniam eos cupiditate ab sequi earum. Sit beatae voluptatem excepturi nobis consequatur repellat itaque. Assumenda ducimus dolorem non a explicabo voluptas perferendis. Voluptatibus voluptatibus eaque ipsum voluptatem asperiores praesentium.', 2, 6, 1, '2019-02-20 12:06:51', '2019-02-20 12:06:51'),
	(2, 'Deleniti in quia aut et.', 14.00, 112.00, 5, 'Nihil voluptatum iste quia doloribus in. Ea et ea consequatur incidunt molestiae voluptas. Quo vitae quibusdam atque amet odio quia.', 2, 7, 1, '2019-02-20 12:06:51', '2019-02-20 12:06:51'),
	(3, 'Dolorem soluta et autem.', 77.00, 195.00, 4, 'Nemo molestias reiciendis est debitis quod. Non odit dolorum neque voluptates. Error omnis et assumenda consequatur sed quis. Quibusdam dignissimos consectetur veritatis facere sint qui.', 3, 4, 2, '2019-02-20 12:06:51', '2019-02-20 12:06:51'),
	(4, 'Sapiente dolores ipsum placeat esse.', 13.00, 127.00, 1, 'Id eius est tenetur ut iste ea. Necessitatibus illo tenetur numquam iusto. Esse voluptate incidunt et.', 3, 3, 5, '2019-02-20 12:06:51', '2019-02-20 12:06:51'),
	(5, 'Omnis nihil sit sapiente.', 90.00, 224.00, 9, 'Sunt aliquid in amet minima consectetur quasi. Explicabo et sunt eos omnis. Molestiae et quaerat quo deserunt consequuntur.', 2, 1, 1, '2019-02-20 12:06:51', '2019-02-20 12:06:51'),
	(6, 'Lorem Ipsum', 45.00, 90.00, 3, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec nec nisl efficitur, mollis ipsum quis, semper felis. Aenean nisl ligula, gravida sed mauris ut, posuere vulputate est. Curabitur et lacinia libero. Suspendisse eu eleifend nulla. Sed facilisis dapibus iaculis. Phasellus urna mi, commodo sed tincidunt tristique, euismod vel est. Cras vel urna ligula. Etiam pretium mi ut libero bibendum aliquet. Etiam pellentesque sapien non lacus imperdiet finibus. Ut efficitur arcu nibh. Phasellus ultricies semper mauris. Vestibulum rhoncus a ante at vulputate. Vestibulum mattis, urna faucibus commodo scelerisque, ex orci ornare lectus, ultrices dapibus sapien quam nec mi. Curabitur malesuada orci ac orci hendrerit, in accumsan erat interdum.', 1, 8, 2, '2019-02-20 12:11:59', '2019-02-20 12:11:59');
/*!40000 ALTER TABLE `rooms` ENABLE KEYS */;

-- Listage de la structure de la table tp-laravel-airbnb. types
CREATE TABLE IF NOT EXISTS `types` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `label` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table tp-laravel-airbnb.types : ~2 rows (environ)
/*!40000 ALTER TABLE `types` DISABLE KEYS */;
INSERT INTO `types` (`id`, `label`, `created_at`, `updated_at`) VALUES
	(1, 'Logement entier', NULL, NULL),
	(2, 'Chambre privée', '2019-02-20 12:56:58', NULL),
	(3, 'Chambre partagée', '2019-02-20 12:57:05', NULL);
/*!40000 ALTER TABLE `types` ENABLE KEYS */;

-- Listage de la structure de la table tp-laravel-airbnb. users
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `role_id` int(10) unsigned,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`),
  KEY `users_role_id_foreign` (`role_id`),
  CONSTRAINT `users_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table tp-laravel-airbnb.users : ~6 rows (environ)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`, `role_id`) VALUES
	(1, 'admin', 'admin@admin.fr', NULL, '$2y$10$Ji3FNyB3QeFEXXQQi8QuxOFM.G5XUNmPy60SNwu/2q60qP2w32esW', 'vOYlyDuYHq9FDjIsbx96pW0bIWyA9BjH1bSn4A5eosbn5djNhryfp6Vcs2wI', '2019-02-20 11:52:42', '2019-02-20 11:52:42', 1),
	(2, 'alicia', 'alicia@alicia.com', NULL, '$2y$10$A24z8o.K/FEJ1BjaHrPycOKaQVeWJg5lUKvPSrcv0kd7HgKcP.xvW', NULL, '2019-02-20 11:59:45', '2019-02-20 11:59:45', 1),
	(3, 'Hayley Thompson', 'kelton.bechtelar@example.com', '2019-02-20 12:01:16', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'a7kWm1tIBy', '2019-02-20 12:01:16', '2019-02-20 12:01:16', 2),
	(4, 'Clovis Blick PhD', 'daija.murray@example.org', '2019-02-20 12:01:16', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', '1c2lHwVqdl', '2019-02-20 12:01:16', '2019-02-20 12:01:16', 1),
	(5, 'Devonte Pollich', 'ykiehn@example.net', '2019-02-20 12:01:16', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'LbttgiacYI', '2019-02-20 12:01:16', '2019-02-20 12:01:16', 2),
	(6, 'Orlando Pacocha', 'greenholt.daniella@example.org', '2019-02-20 12:01:16', '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', 'o9MuOIXjT6', '2019-02-20 12:01:16', '2019-02-20 12:01:16', 2);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
