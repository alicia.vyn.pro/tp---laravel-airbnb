@extends('layouts.app')

@section('title', 'Réservations')

@section('header')
    <h1>Vos réservations</h1>
@endsection

@section('content')
Vos prochaines vacances :
<table class="table">
    <thead class="thead-dark">
    <tr>
        <th scope="col">Location</th>
        <th scope="col">Ville</th>
        <th scope="col">Pays</th>
        <th scope="col">Date de début</th>
        <th scope="col">Date de fin</th>
    </tr>
    </thead>

    <tbody>
        @foreach($reservations as $reservation)
            <tr>
                <td>{{ $reservation->room->title }}</td>
                <td>{{ $reservation->room->address->ville }}</td>
                <td>{{ $reservation->room->address->pays }}</td>
                <td>{{ $reservation->date_start }}</td>
                <td>{{ $reservation->date_end }}</td>
            </tr>
        @endforeach
    </tbody>
</table>

@if(Auth::user()->role_id == 1)
    Vos chambres réservées :
    <table class="table">
        <thead class="thead-dark">
            <tr>
                <th scope="col">Location</th>
                <th scope="col">Ville</th>
                <th scope="col">Pays</th>
                <th scope="col">Date de début</th>
                <th scope="col">Date de fin</th>
            </tr>
        </thead>
    <tbody>
    @foreach( Auth::user()->rooms as $room)

            @foreach( $room->reserved as $reservation )

                <tr>
                    <td>{{ $reservation->room->title }}</td>
                    <td>{{ $reservation->room->address->ville }}</td>
                    <td>{{ $reservation->room->address->pays }}</td>
                    <td>{{ $reservation->date_start }}</td>
                    <td>{{ $reservation->date_end }}</td>
                </tr>

            @endforeach

    @endforeach
    </tbody>
</table>
@endif


@endsection