@extends('layouts.app')

@section('title', 'Ajouter une chambre')

@section('header')
    <h1>Ajouter une chambre</h1>
@endsection

@section('content')
    @include('errors')

     <form action="{{ route('rooms.store') }}" method="POST">

         {{-- Champs de protection CSRF (obligatoire avec Laravel) --}}
         @csrf

         <!-- Titre annonce -->
         <div class="form-row">
             <div class="col-md-12 mb-3">
                 <label>Titre de votre annonce</label>
                 <input type="text" name="title" class="form-control" value="{{ old('title') }}">
             </div>
         </div>

         <!-- Taille / Prix / Couchage -->
         <div class="form-row">
             <div class="col-md-4 mb-3">
                 <label>Taille</label>
                 <div class="input-group">
                     <input type="number" step="0.5" name="size" class="form-control" value="{{ old('size') }}">
                     <div class="input-group-prepend">
                         <span class="input-group-text">m²</span>
                     </div>
                 </div>
             </div>
             <div class="col-md-4 mb-3">
                 <label>Prix pour la nuit</label>
                 <div class="input-group">
                     <input type="number" step="0.01" name="price" class="form-control" value="{{ old('price') }}">
                     <div class="input-group-prepend">
                         <span class="input-group-text">€</span>
                     </div>
                 </div>
             </div>
             <div class="col-md-4 mb-3">
                 <label>Nombre de couchages</label>
                 <input type="number" step="1" name="bedding" class="form-control" value="{{ old('bedding') }}">
             </div>
         </div>

         <!-- Num rue / Adresse complète -->
         <div class="form-row">
             <div class="col-md-3 mb-3">
                 <label>Numéro</label>
                 <input type="text" name="numero" class="form-control" value="{{ old('numero') }}">
             </div>
             <div class="col-md-9 mb-3">
                 <label>Adresse complète</label>
                 <input type="text" name="rue" class="form-control" value="{{ old('rue') }}">
             </div>
         </div>

         <!-- CP / Ville / Pays -->
         <div class="form-row">
             <div class="col-md-3 mb-3">
                 <label>Code Postal</label>
                 <input type="number" step="1" name="cp" class="form-control" value="{{ old('cp') }}">
             </div>
             <div class="col-md-6 mb-3">
                 <label>Ville</label>
                 <input type="text" name="ville" class="form-control" value="{{ old('ville') }}">
             </div>
             <div class="col-md-3 mb-3">
                 <label>Pays</label>
                 <input type="text" name="pays" class="form-control" value="{{ old('pays') }}">
             </div>
         </div>

         <!-- Description -->
         <div class="form-group">
             <label>Description</label>
             <textarea name="description" class="form-control" rows="5">{{ old('description') }}</textarea>
         </div>

         <!-- Type logement -->
         <div class="form-group">
             <label>Type de location</label>
             <select name="type_id" class="custom-select">
                 <option selected>Selectionner un type de location</option>
                 @foreach ($types as $type )
                 <option value="{{ $type->id }}">{{ $type->label }}</option>
                 @endforeach
             </select>
         </div>

         <!-- Equipements -->
         <div class="form-group">
             <label>Equipements</label>
             <br>
             @foreach ($equipments as $equipment )
             <div class="form-check form-check-inline">
                 <input name="equipments[]" class="form-check-input" type="checkbox" value="{{ $equipment->id }}">
                 <label class="form-check-label" >{{ $equipment->label }}</label>
             </div>
             @endforeach
         </div>

        <button class="btn btn-primary" type="submit">Envoyer</button>
    </form>

@endsection
