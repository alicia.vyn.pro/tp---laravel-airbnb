@extends('layouts.app')

@section('title', 'Chambres')

@section('header')
    <h1>Toutes les chambres</h1>
@endsection

@section('content')

    <div>

        <h2>{{ $room->title }}</h2>
        <span>Proposé par {{ $room->user->name  }}</span>
        <hr>
        <span class="h3">{{ $room->type->label }}</span>
        <br>
        <span class="h4">{{ $room->size }}m²</span> -
        <span class="h4">{{ $room->price }}€ la nuit</span>
        <p>{{ $room->description }}</p>
        <p>{{ $room->bedding }} couchages</p>

        <p>Situé au
            {{ $room->address->numero }}
            {{ $room->address->rue }} -
            {{ $room->address->cp }}
            {{ $room->address->ville }},
            {{ $room->address->pays }}
        </p>

        <p>Equipements disponibles : </p>
            <span>
                @foreach ($equips as $equip)
                    {{ $room->hasEquipment( $equip->id ) ? $equip->label : '' }}
                @endforeach
            </span>

    </div>

    <hr>

    @auth
        <div>
        <form action="{{ route('reservations.store', $room) }}" method="POST">

            {{-- Champs de protection CSRF (obligatoire avec Laravel) --}}
            @csrf

            <div class="text-center">
                <div class="h3">Reservation</div>
                du
                <input type="date"
                       name="date_start"
                       value="{{ date('Y-m-d') }}">
                au
                <input type="date"
                       name="date_end"
                       value="{{ date('Y-m-d', strtotime( '+1 week' )) }}">

                <br><br>
                <button class="btn btn-primary" type="submit">Réserver cette location</button>
            </div>
        </form>
    </div>
    @endauth
@endsection