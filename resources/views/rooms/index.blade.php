@extends('layouts.app')

@section('title', 'Chambres')

@section('header')
    <h1>Toutes les chambres</h1>

@endsection

@section('content')

    <div class="card-columns">
        @foreach ($rooms as $room)
            <div class="card">
                <div class="card-body">
                    <h2 class="card-title">{{ $room->title }} </h2>
                    <hr>
                    <h3 class="card-title">{{ $room->size }} m²</h3>
                    <h3 class="card-title">{{ $room->price }}€ la nuit</h3>
                    <p class="card-text">{{ $room->bedding }} couchages</p>
                    <p class="card-text text-justify">{{ $room->description }}</p>
                <a href="{{ route('rooms.show', $room) }}" class="btn btn-primary mb-md-3">Voir la location</a>
                </div>
            </div>
        @endforeach
    </div>

@endsection